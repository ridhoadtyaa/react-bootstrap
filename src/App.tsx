import Content from './components/Content';
import Footer from './components/Footer';
import Header from './components/Header';

const App = () => {
	return (
		<>
			<Header />
			<main className="container">
				<Content />
			</main>
			<Footer />
		</>
	);
};

export default App;
